import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
            <View style={styles.profil}>
              <Icon name="account" size={100} style={{color: '#707b7c'}} />
            </View>
            <Image source={require('../images/logo-black.png')} style={styles.logoImage} />
            <Text style={{color: '#707b7c'}}>V1.0</Text>
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="home" size={25} style={{color: '#707b7c'}} />
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('AboutScreen')}>
            <Icon name="account" size={25} style={{color: '#707b7c'}} />
            <Text style={styles.tabTitle}>About</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  profil: {
    borderColor: '#707b7c',
    borderWidth: 3,
    borderRadius: 100,
    padding: 10
  },
  logoImage: {
    width: Dimensions.get('window').width - 250,
    resizeMode: "contain",
    height: 30,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 50,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c'
  }
});