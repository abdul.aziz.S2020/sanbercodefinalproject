import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    this.props.navigation.navigate('HomeScreen', { name: 'Aziz' });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.logo}>
            <Image source={require('../images/logo-white.png')} style={styles.logoImage} />
          </View>
          <View style={styles.slider}>
            <Image source={require('../images/bg-1.jpg')} style={styles.sliderImage} />
          </View>
          <View style={styles.formLogin}>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Username / Email</Text>
              <TextInput
                style={styles.input}
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Password</Text>
              <TextInput
                style={styles.input}
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
            <View style={styles.formGroup}>
              <TouchableOpacity style={styles.button} onPress={() => this.loginHandler()}>
                <Text style={styles.buttonText}>Login</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginBottom: 15, alignItems: 'center'}}>
              <Text style={{fontSize: 14, color: '#E23744'}}>Don't have an account? Signup.</Text>
            </View>
            <View style={styles.formGroup}>
              <View style={styles.social}>
                <TouchableOpacity style={styles.socialButton}>
                  <Icon name="facebook" size={20} style={styles.icon} />
                  <Text style={styles.socialButtonText}>Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.socialButton}>
                  <Icon name="google" size={20} style={styles.icon} />
                  <Text style={styles.socialButtonText}>Google</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 15
  },
  label: {
    color: '#707b7c',
  },
  icon: {
    color: '#707b7c',
    marginHorizontal: 5
  },
  button: {
    backgroundColor: '#E23744',
    borderRadius: 5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize:16,
    color:'white'
  },
  logo: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#E23744',
    marginVertical: 40
  },
  logoImage: {
    width: Dimensions.get('window').width - 250,
    resizeMode: "contain",
    height: 30,
  },
  slider: {
    marginBottom: 20
  },
  sliderImage: {
    width: Dimensions.get('window').width,
    resizeMode: "contain",
    height: 150,
  },
  formLogin: {
    alignSelf: 'stretch'
  },
  formGroup: {
    marginBottom: 15,
  },
  input: {
    height: 40,
    borderColor: '#707b7c',
    borderWidth: 1,
    borderRadius: 5
  },
  social: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  socialButton: {
    height: 40,
    width: '48%',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#707b7c',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
  },
  socialButtonText: {
    color: '#707b7c',
    fontSize: 16
  },
});