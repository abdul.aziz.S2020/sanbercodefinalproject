import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';

export default class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataRestaurant: {},
      isLoadingRestaurant: true,
      isErrorRestaurant: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getRestaurant();
  }

  //   Get Api Restaurant
  getRestaurant = async () => {
    let result;
    try {
      let response = await Axios.request({
        method: 'GET',
        baseURL: 'https://developers.zomato.com/api/v2.1/restaurant?res_id=16774318',
        headers: {
          'Content-Type': 'application/json',
          'user-key': "147430113a28575f415b5505fef73eb2"
        },
      });

      //const response = await Axios.get(`https://api.github.com/users?since=135`)
      this.setState({ isErrorRestaurant: false, isLoadingRestaurant: false, dataRestaurant: response.data})
    } catch (error) {
      this.setState({ isLoadingRestaurant: false, isErrorRestaurant: true })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <ImageBackground source={{uri: 'https://images.unsplash.com/photo-1514933651103-005eec06c04b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80'}} style={styles.contentImage}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-left" size={25} style={{color:'#fff'}} />
            </TouchableOpacity>
          </ImageBackground>
        </View>
        <View style={styles.body}>
          <View style={styles.description}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>Nama Restaurant</Text>
            <Text style={styles.contentText}>Category</Text>
            <Text style={styles.contentText}>Location</Text>
          </View>
          <View style={styles.address}>
            <Text style={{fontSize: 16, marginBottom:10}}>ADDRESS</Text>
            <Text style={styles.contentText}>Jl. Perumahan Serpong, Tangerang</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    backgroundColor: 'white',
  },
  contentImage: {
    resizeMode: "cover",
    height: 250,
    paddingHorizontal: 5,
    paddingVertical: 20,
    elevation: 3
  },
  contentText: {
    color: '#707b7c'
  },
  description: {
    padding: 15,
    backgroundColor: 'white',
    elevation: 3,
  },
  address: {
    padding: 15,
    backgroundColor: 'white'
  }
});