import React, { Component } from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "./LoginScreen";
import HomeScreen from "./HomeScreen";
import DetailScreen from "./DetailScreen";
import AboutScreen from "./AboutScreen";

const AuthStack = createStackNavigator();
const LoginStack = createStackNavigator();
const HomeStack = createStackNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator
    screenOptions={{
      headerShown: false
    }}>
    <LoginStack.Screen name="LoginScreen" component={LoginScreen} options={{title: "Login Screen"}} />
    <LoginStack.Screen name="HomeScreen" component={HomeStackScreen} options={{title: "Home Screen"}} />
  </LoginStack.Navigator>
);

const HomeStackScreen = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerShown: false
    }}>
    <HomeStack.Screen name="HomeScreen" component={HomeScreen} options={{title: "Home Screen"}} />
    <HomeStack.Screen name="DetailScreen" component={DetailScreen} options={{title: "Detail Screen"}} />
    <HomeStack.Screen name="LoginScreen" component={LoginScreen} options={{title: "Login Screen"}} />
    <HomeStack.Screen name="AboutScreen" component={AboutScreen} options={{title: "About Screen"}} />
  </HomeStack.Navigator>
);

export default () => (
    <NavigationContainer>
      <AuthStack.Navigator
        screenOptions={{
          headerShown: false
        }}>
        <AuthStack.Screen name="LoginScreen" component={LoginStackScreen} options={{title: "Login Screen"}} />
      </AuthStack.Navigator>
    </NavigationContainer>
  );
  