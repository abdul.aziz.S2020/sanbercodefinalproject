import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataRestaurant: {},
      isLoadingRestaurant: true,
      isErrorRestaurant: false,
      dataCategory: {},
      isLoadingCategory: true,
      isErrorCategory: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getRestaurant();
    this.getCategory();
  }

  //   Get Api Restaurant
  getRestaurant = async () => {
    let result;
    try {
      let response = await Axios.request({
        method: 'GET',
        baseURL: 'https://developers.zomato.com/api/v2.1/search',
        headers: {
          'Content-Type': 'application/json',
          'user-key': "a31bd76da32396a27b6906bf0ca707a2",
        },
      });

      //const response = await Axios.get(`https://api.github.com/users?since=135`)
      this.setState({ isErrorRestaurant: false, isLoadingRestaurant: false, dataRestaurant: response.data.restaurants})
    } catch (error) {
      this.setState({ isLoadingRestaurant: false, isErrorRestaurant: true })
    }
  }

  //   Get Api Category
  getCategory = async () => {
    try {
      let response = await Axios.request({
        method: 'GET',
        baseURL: 'https://developers.zomato.com/api/v2.1/categories',
        headers: {
          'Content-Type': 'application/json',
          'user-key': "a31bd76da32396a27b6906bf0ca707a2",
        },
      });

      //const response = await Axios.get(`https://api.github.com/users?since=135`)
      this.setState({ isErrorCategory: false, isLoadingCategory: false, dataCategory: response.data.categories})
    } catch (error) {
      this.setState({ isLoadingCategory: false, isErrorCategory: true })
    }
  }

  detail(idDetail) {
    this.props.navigation.navigate('DetailScreen', { id: idDetail });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <View style={styles.topBar}>
            <View style={styles.leftNav}>
              <Icon name="map-marker-outline" size={30} style={styles.icon} />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>Jakarta</Text>
            </View>
            <View style={styles.rightNav}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}>
                <Icon style={styles.icon} name="qrcode-scan" size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.searchBar}>
            <TextInput
              style={styles.input}
              placeholder="Search"
            />
          </View>
          <View style={styles.category}>
          {
              this.state.isLoadingCategory ?
                <View
                  style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                  <ActivityIndicator size='large' color='red' />
                </View> :
              (
                this.state.isErrorCategory ?
                  <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                  >
                    <Text>Terjadi Error Saat Memuat Data</Text>
                  </View> :
                (
                  this.state.dataCategory.length == 0 ?
                    <View style={{ flex: 1, padding: 20 }}>
                      <Text style={{ color: '#000', fontWeight: 'bold' }}>No restaurants from selected category</Text>
                    </View> :
                    <FlatList
                    horizontal={true}
                    scrollEventThrottle={1900} 
                    data={this.state.dataCategory}
                    renderItem={({ item }) =>
                      <View style={styles.categoryItem}>
                        <Text style={styles.categoryText}>{item.categories.name}</Text>
                      </View>
                    }
                    keyExtractor={({ id }, index) => index}
                  />
                )
              )
            }
          </View>
        </View>
        <ScrollView>
          <View style={styles.body}>
            {
              this.state.isLoadingRestaurant ?
                <View
                  style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                >
                  <ActivityIndicator size='large' color='red' />
                </View> :
              (
                this.state.isErrorRestaurant ?
                  <View
                    style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
                  >
                    <Text>Terjadi Error Saat Memuat Data</Text>
                  </View> :
                (
                  this.state.dataRestaurant.length == 0 ?
                    <View style={{ flex: 1, padding: 20 }}>
                      <Text style={{ color: '#000', fontWeight: 'bold' }}>No restaurants from selected category</Text>
                    </View> :
                    <FlatList
                    style={styles.content}
                    data={this.state.dataRestaurant}
                    renderItem={({ item }) =>
                      <TouchableOpacity style={styles.contentItem} onPress={() => this.detail(item.restaurant.id)}>
                        <ImageBackground source={{uri: `${item.restaurant.thumb}`}} style={styles.contentImage} />
                        <View style={styles.contentDesc}>
                          <Text style={{fontSize: 16, fontWeight: 'bold'}}>{item.restaurant.name}</Text>
                          <Text style={styles.contentText}>{`${item.restaurant.establishment} - ${item.restaurant.cuisines}`}</Text>
                          <Text style={styles.contentText}>{`${item.restaurant.location.locality}`}</Text>
                          <Text style={styles.contentText}>{`${item.restaurant.currency}${item.restaurant.average_cost_for_two} for two`}</Text>
                        </View>
                      </TouchableOpacity>
                    }
                    keyExtractor={({ id }, index) => index}
                  />
                )
              )
            }
          </View>
        </ScrollView>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="home" size={25} style={{color: '#707b7c'}} />
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('AboutScreen')}>
            <Icon name="account" size={25} style={{color: '#707b7c'}} />
            <Text style={styles.tabTitle}>About</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 15
  },
  label: {
    color: '#003366',
  },
  icon: {
    color: '#424949',
    marginHorizontal: 5
  },
  button: {
    backgroundColor: '#E23744',
    borderRadius: 5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize:16,
    color:'white'
  },
  input: {
    height: 30,
    borderColor: '#707b7c',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 5
  },
  sliderImage: {
    width: Dimensions.get('window').width,
    resizeMode: "contain",
    height: 150,
  },
  navBar: {
    backgroundColor: 'white',
    elevation: 1,
    paddingHorizontal: 15,
    paddingTop: 20
  },
  topBar: {
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  leftNav: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25
  },
  searchBar: {
    marginBottom: 15
  },
  category: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  categoryItem: {
    padding: 4,
    marginRight: 5,
    paddingHorizontal: 8,
    borderColor: '#707b7c',
    borderWidth: 1,
    borderRadius: 5,
  },
  categoryText: {
    color: '#707b7c',
    fontSize: 12
  },
  content: {
    alignSelf: 'stretch'
  },
  contentItem: {
    alignSelf: 'stretch',
    borderColor: '#707b7c',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 15
  },
  contentImage: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    height: 150
  },
  contentText: {
    color: '#707b7c'
  },
  contentDesc: {
    padding: 15
  },
  tabBar: {
    backgroundColor: 'white',
    height: 50,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 11,
    color: '#707b7c'
  }
});